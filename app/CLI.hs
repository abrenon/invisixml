module CLI (
      Command(..)
    , Input(..)
    , getCommand
  ) where

import Data.Version (showVersion)
import Control.Applicative ((<*>), optional)
import Options.Applicative (
      Parser, ReadM, argument, execParser, fullDesc, header
    , help, helper, info, long, metavar, option, short, str, switch, value
  )
import qualified Paths_InvisiXML as InvisiXML (version)

data Input = StdIn | FileInput FilePath

data Command = Command {
      input :: Input
    , outputPrefix :: Maybe FilePath
    , pristine :: Bool
  }

inputArg :: ReadM Input
inputArg = fileOrStdIn <$> str
  where
    fileOrStdIn "-" = StdIn
    fileOrStdIn f = FileInput f

command :: Parser Command
command = Command
  <$> argument inputArg (metavar "INPUT_FILE" <> value StdIn
          <> help "XML file to process"
        )
  <*> option (optional str) (short 'o' <> long "outputPrefix" <> value Nothing
          <> help "prefix for the output files"
        )
  <*> switch (short 'p' <> long "pristine"
          <> help "keep input exactly as is, not unindenting or normalizing it in any way"
        )

getCommand :: IO Command
getCommand = execParser $
  info
    (helper <*> command)
    (fullDesc <> header ("InvisiXML v" ++ showVersion InvisiXML.version))
