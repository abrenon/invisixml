{-# LANGUAGE NamedFieldPuns #-}
module Main where

import CLI (Command(..), Input(..), getCommand)
import Control.Monad.Except (runExceptT)
import Data.Maybe (fromMaybe)
import Data.Text (Text)
import qualified Data.Text.IO as Text (getContents, readFile, writeFile)
import Text.InvisiXML as Parser (InvisiXML(..), ParsingConfig(..), parse)
import Text.XML.Light.Serializer (encode)
import System.FilePath ((<.>), dropExtension)
import System.Exit (die)

endPoints :: Command -> IO (IO Text, FilePath)
endPoints (Command {input = StdIn, outputPrefix}) =
  (,) Text.getContents <$> maybe noOutputPrefix return outputPrefix
  where
    noOutputPrefix = die "output prefix (-o) is necessary when running on stdin"
endPoints (Command {input = FileInput f, outputPrefix}) =
  return (Text.readFile f, fromMaybe (dropExtension f) outputPrefix)

run :: Command -> IO ()
run command = do
  (source, prefix) <- endPoints command
  source >>= runExceptT . parse parsingConfig
    >>= either (fail . show) (create prefix)
  where
    parsingConfig = ParsingConfig {Parser.pristine = CLI.pristine command}
    create prefix (InvisiXML {text, structure}) = do
      Text.writeFile (prefix <.> "txt") text
      writeFile (prefix <.> "ixml") $ encode structure

main :: IO ()
main = getCommand >>= run
