{-# LANGUAGE NamedFieldPuns #-}
{-# LANGUAGE FlexibleContexts #-}
module Data.PositionTree (
      Node(..)
    , Position(..)
    , PositionTree(..)
    , addSibling
    , empty
    , merge
    , offset
    , origin
  ) where

import Control.Monad (foldM)
import Control.Monad.Except (MonadError(..))
import Data.Map as Map (Map, alter, foldrWithKey)
import qualified Data.Map as Map (empty)
import qualified Data.Map.Merge.Strict as Map (merge, preserveMissing, zipWithMatched)
import Data.Text as Text (Text, length)
import Text.XML.Light (Content(..), Element(..))
import Text.XML.Light.Serializer (
    FromXML(..), ToXML(..), (.=), expectElement, readAttr
  )
import Text.InvisiXML.Error (ParsingError(..), StructureError)
import Text.InvisiXML.Namespace (addAttr, ixml, setChildren)
import Text.Read (readPrec)

newtype Position = Position {
    getPosition :: Int
  } deriving (Eq, Ord)

instance Show Position where
  show = show . getPosition

instance Read Position where
  readPrec = Position <$> readPrec

origin :: Position
origin = Position 0

offset :: Text -> Position -> Position
offset input (Position from) = Position (from + Text.length input)

data Node a =
    Point a
  | Range {
          to :: Position
        , value :: a
        , children :: PositionTree a
      }
  deriving (Show)

instance FromXML a => FromXML (Node a) where
  fromXML = expectElement "Node" $ \e@(Element {elContent}) ->
    let value = fromXML [Elem e] in
    (Range <$> (readAttr "Position" qTo e) <*> value <*> fromXML elContent)
    `catchError` (pointIfNoTo value)
    where
      qTo = ixml "to"
      pointIfNoTo value e@(MissingAttribute {})
        | missingQName e == qTo = Point <$> value
      pointIfNoTo _ e = throwError e

instance ToXML a => ToXML (Node a) where
  toXML (Point p) = toXML p
  toXML (Range {to, value, children}) =
    setChildren (toXML children) . addAttr (ixml "to" .= to) <$> toXML value

newtype PositionTree a = PositionTree (Map Position [Node a]) deriving (Show)

instance FromXML a => FromXML (PositionTree a) where
  fromXML contents = foldM addNode empty [e | (Elem e) <- contents]
    where
      addNode positionTree e = addSibling
        <$> readAttr "Position" (ixml "at") e
        <*> fromXML [Elem e]
        <*> return positionTree

instance ToXML a => ToXML (PositionTree a) where
  toXML (PositionTree m) = foldrWithKey nodesToXML [] m
    where
      nodesToXML at nodes l =
        (addAttr (ixml "at" .= at) <$> (toXML =<< nodes)) ++ l

addSibling :: Position -> Node a -> PositionTree a -> PositionTree a
addSibling at node (PositionTree m) = PositionTree $ alter pushNode at m
  where
    pushNode Nothing = Just [node]
    pushNode (Just l) = Just (l ++ [node])

empty :: PositionTree a
empty = PositionTree Map.empty

merge :: MonadError StructureError m =>
  PositionTree a -> PositionTree a -> m (PositionTree a)
merge (PositionTree m0) (PositionTree m1) = return . PositionTree $
  Map.merge
    Map.preserveMissing
    Map.preserveMissing
    (Map.zipWithMatched undefined)
    m0
    m1
