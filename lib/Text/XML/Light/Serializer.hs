{-# LANGUAGE FlexibleContexts #-}
module Text.XML.Light.Serializer (
      FromXML(..)
    , ToXML(..)
    , decode
    , encode
    , (.=)
    , expectElement
    , readAttr
  ) where

import Control.Monad.Except (MonadError(..))
import Data.List (find, intercalate)
import Text.Read (readEither)
import Text.XML.Light (
    Attr(..), Content(..), Element(..), QName, parseXML, ppContent
  )
import Text.XML.Light.Lexer (XmlSource)
import Text.InvisiXML.Error (ParsingError(..))

class FromXML a where
  fromXML :: MonadError ParsingError m => [Content] -> m a

class ToXML a where
  toXML :: a -> [Content]

decode :: (XmlSource s, FromXML a, MonadError ParsingError m) => s -> m a
decode = fromXML . parseXML

encode :: ToXML a => a -> String
encode = intercalate "\n" . fmap ppContent . toXML

(.=) :: Show v => QName -> v -> Attr
k .= v = Attr k $ show v

expectElement :: MonadError ParsingError m =>
  String -> (Element -> m a) -> [Content] -> m a
expectElement _ parser [Elem e] = parser e
expectElement dataName _ [c] = throwError $ UnexpectedContent dataName c
expectElement dataName _ l = throwError . WrongNumber dataName 1 $ length l

readAttr :: (MonadError ParsingError m, Read a) => String -> QName -> Element -> m a
readAttr dataName key =
  maybe (throwError $ MissingAttribute dataName key)
        (either (throwError . BadAttribute dataName key)
                return
          . readEither . attrVal)
  . find ((==) key . attrKey) . elAttribs
