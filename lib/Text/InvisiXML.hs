{-# LANGUAGE NamedFieldPuns #-}
{-# LANGUAGE FlexibleContexts #-}
module Text.InvisiXML (
      InvisiXML(..)
    , ParsingConfig(..)
    , Structure(..)
    , merge
    , parse
  ) where

import Control.Monad.Except (MonadError(..))
import Control.Monad.RWS (RWST(..), execRWST, asks, gets, modify, state)
import Data.Char (isSpace)
import Data.List (uncons)
import Data.PositionTree as PositionTree (
      Node(..), PositionTree, Position, addSibling, children, empty
    , offset, origin
  )
import qualified Data.PositionTree as PositionTree (merge)
import Data.Text (Text)
import qualified Data.Text as Text (concat, null, pack)
import Text.InvisiXML.Error (
    ParsingError(..), TagOccurrence(..), StructureError(..), XMLError(..)
  )
import Text.InvisiXML.Namespace (prefix, uRI, ixml)
import Text.XML.Light (
    Attr(..), Content(..), Element(..), QName(..), node, showCData, showContent
  )
import Text.XML.Light.Lexer (Token(..), XmlSource, tokens)
import Text.XML.Light.Serializer (FromXML(..), ToXML(..), expectElement)

data FrozenElement = FrozenElement {
      frozenName :: QName
    , frozenAttrs :: [Attr]
  } deriving (Show)

instance FromXML FrozenElement where
  fromXML = expectElement "FrozenElement" $ \(Element {elName, elAttribs}) ->
    return $ FrozenElement {frozenName = elName, frozenAttrs = elAttribs}

instance ToXML FrozenElement where
  toXML (FrozenElement {frozenName, frozenAttrs}) =
    [Elem Element {
          elName = frozenName
        , elAttribs = frozenAttrs
        , elContent = []
        , elLine = Nothing
      }]

data Structure = Structure {
    positionTree :: PositionTree FrozenElement
  } deriving (Show)

instance FromXML Structure where
  fromXML = expectElement "Structure" parseStructure
    where
      parseStructure e
        | elName e == ixml "structure" = Structure <$> fromXML (elContent e)
        | otherwise = throwError . UnexpectedContent "Structure" $ Elem e

instance ToXML Structure where
  toXML (Structure s) = [Elem $ node (ixml "structure") ([Attr ns uRI], toXML s)]
    where
      ns = QName prefix (Just uRI) (Just "xmlns")

onTree :: (PositionTree FrozenElement -> PositionTree FrozenElement) -> Structure -> Structure
onTree f = Structure . f . positionTree

data InvisiXML = InvisiXML {
      structure :: Structure
    , text :: Text
  }

type Context = Maybe TagOccurrence

data ParsingState = ParsingState {
      input :: [Token]
    , at :: Position
    , context :: Context
    , stack :: [Text]
    , subStructure :: Structure
  }

data ParsingConfig = ParsingConfig {
    pristine :: Bool
  }

openStream :: [Token] -> ParsingState
openStream input = ParsingState {
      input
    , at = origin
    , context = Nothing
    , stack = []
    , subStructure = Structure empty
  }

type Parser = RWST ParsingConfig () ParsingState

pop :: Monad m => Parser m (Maybe Token)
pop = gets (uncons . input) >>= updateState
  where
    updateState Nothing = return Nothing
    updateState (Just (t, input)) = 
      state $ \parsingState -> (Just t, parsingState {input})

appendText :: Monad m => String -> Parser m ()
appendText s = asks (toText . pristine) >>= modify . append
  where
    toText b = Text.pack $ (if b then id else unindent) s
    unindent ('\n':s') = '\n':(unindent $ dropWhile isSpace s')
    {-
      case dropWhile isSpace s' of
        [] -> []
        s2 -> '\n':(unindent s2)
        -}
    unindent (c:s') = c:(unindent s')
    unindent [] = []
    append t parsingState
      | Text.null t = parsingState
      | otherwise = parsingState {
              at = offset t $ at parsingState
            , stack = t : stack parsingState
          }

appendNode :: Monad m => Maybe Position -> Node FrozenElement -> Parser m ()
appendNode forcedAt n = modify $ \parsingState@(ParsingState {at}) ->
  let position = maybe at id forcedAt in
  parsingState {
      subStructure = addSibling position n `onTree` subStructure parsingState
    }

enter :: Monad m => TagOccurrence -> Parser m (Position, (Context, Structure))
enter newTag = state $ \parsingState -> (
      (at parsingState, (context parsingState, subStructure parsingState))
    , parsingState {context = Just newTag, subStructure = Structure empty}
  )

restore :: Monad m => (Context, Structure) -> Parser m Position
restore (context, subStructure) = state $ \parsingState ->
  (at parsingState, parsingState {context, subStructure})

checkout :: MonadError XMLError m => Context -> Parser m ()
checkout actual = gets context >>= compareWith actual
  where
    compareWith (Just tO) Nothing = throwError $ ClosingUnopen tO
    compareWith Nothing (Just tO) = throwError $ Unclosed tO
    compareWith (Just tO0) (Just tO1)
      | tag tO0 /= tag tO1 = throwError $ Mismatch {open = tO0, close = tO1}
    compareWith _ _ = return ()

parse :: (XmlSource s, MonadError XMLError m) =>
  ParsingConfig -> s -> m InvisiXML
parse config =
  fmap collectState . execRWST fillStructure config . openStream . tokens
  where
    collectState (ParsingState {stack, subStructure}, _) = InvisiXML {
          structure = subStructure
        , text = Text.concat $ reverse stack
      }

fillStructure :: MonadError XMLError m => Parser m ()
fillStructure = pop >>= maybe (checkout Nothing) handle

handle :: MonadError XMLError m => Token -> Parser m ()
handle (TokStart _ qName attrs True) =
  appendNode Nothing (Point $ FrozenElement qName attrs) *> fillStructure
handle (TokStart line qName attrs False) = do
  (start, current) <- enter $ TagOccurrence {tag = qName, openLine = line}
  fillStructure
  Structure children <- gets subStructure
  to <- restore current
  appendNode (Just start) (Range {to, value = FrozenElement qName attrs, children})
  fillStructure
handle (TokEnd line qName) =
  checkout . Just $ TagOccurrence {tag = qName, openLine = line}
handle (TokCRef s) = appendText (showContent $ CRef s) *> fillStructure
handle (TokText t) = appendText (showCData t) *> fillStructure

merge :: MonadError StructureError m => Structure -> Structure -> m Structure
merge s0 s1 = Structure <$> PositionTree.merge (positionTree s0) (positionTree s1)
