module Text.InvisiXML.Error (
      InvisiXMLError(..)
    , Occurrence(..)
    , ParsingError(..)
    , StructureError(..)
    , TagOccurrence(..)
    , XMLError(..)
  ) where

import Text.XML.Light (Content, Line, QName)

data Occurrence =
    Line Line
  | EOF
  deriving Show

data TagOccurrence = TagOccurrence {
      tag :: QName
    , openLine :: Line
  }
  deriving Show

data XMLError =
    ClosingUnopen TagOccurrence
  | Mismatch {
          open :: TagOccurrence
        , close :: TagOccurrence
      }
  | Unclosed TagOccurrence
  deriving Show

data ParsingError =
    UnexpectedContent {
          expectedData :: String
        , found :: Content
      }
  | BadAttribute {
          expectedData :: String
        , badQName :: QName
        , expectedInfo :: String
      }
  | MissingAttribute {
          expectedData :: String
        , missingQName :: QName
      }
  | WrongNumber {
          expectedData :: String
        , expectedNumber :: Int
        , actualNumber :: Int
      }
  deriving Show

data StructureError =
    DifferentTextError
  | OverlappingStructureError
  deriving Show

data InvisiXMLError =
    XMLError XMLError
  | ParsingError ParsingError
  | StructureError StructureError
  deriving Show
