{-# LANGUAGE NamedFieldPuns #-}
module Text.InvisiXML.Namespace (
      uRI
    , prefix
    , ixml
    , addAttr
    , setChildren
  ) where

import Text.XML.Light (Attr(..), Content(..), Element(..), QName(..), add_attr)

uRI :: String
uRI = "https://gitlab.liris.cnrs.fr/abrenon/InvisiXML"

prefix :: String
prefix = "ixml"

ixml :: String -> QName
ixml qName = QName {qName, qURI = Just uRI, qPrefix = Just prefix}

onContent :: (Element -> Element) -> Content -> Content
onContent f (Elem e) = Elem (f e)
onContent _ x = x

addAttr :: Attr -> Content -> Content
addAttr = onContent . add_attr

setChildren :: [Content] -> Content -> Content
setChildren elContent = onContent $ \e -> e {elContent}
